<?php
/**
 * Hello! Pro 3
 *
 * This file adds functions to the Hello Pro 3 Theme.
 *
 * @package Hello Pro
 * @author  brandiD
 * @license GPL-2.0+
 * @link    http://my.studiopress.com/themes/hello/
 */

// Start Genesis.
require_once get_template_directory() . '/lib/init.php';

// Load constants - use constants in code instead of functions to improve performance. Hat Tip to Tonya at Knowthecode.io.
$child_theme = wp_get_theme( get_stylesheet_directory() );

// Child theme variables (do not remove).
define( 'CHILD_THEME_HANDLE', sanitize_title_with_dashes( wp_get_theme()->get( 'Name' ) ) );
define( 'CHILD_THEME_VERSION', wp_get_theme()->get( 'Version' ) );

// Set up the Theme.
require_once get_stylesheet_directory() . '/lib/theme-defaults.php';

// Set Localization (do not remove).
add_action( 'after_setup_theme', 'hello_pro_localization_setup' );
/**
 * Loads text Domain
 *
 * @since  3.0.1
 */
function hello_pro_localization_setup() {
	load_child_theme_textdomain( 'hello-pro', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'hello-pro' ) );
}

// Add Theme setup functions.
require_once get_stylesheet_directory() . '/lib/theme-setup.php';

// Add the onboarding functions.
require_once get_stylesheet_directory() . '/lib/onboarding-functions.php';

// Add the custom meta boxes.
require_once get_stylesheet_directory() . '/lib/metaboxes.php';

// Import Customizer custom toggle control.
require_once get_stylesheet_directory() . '/lib/class-hello-pro-toggle-control.php';

// Add Customizer settings.
require_once get_stylesheet_directory() . '/lib/customize.php';

// Includes Customizer CSS.
require_once get_stylesheet_directory() . '/lib/output.php';

// Add Gutenberg functions.
add_action( 'after_setup_theme', 'genesis_child_gutenberg_support' );
/**
 * Enqueues the block editor stylesheet.
 */
function genesis_child_gutenberg_support() {
	require_once get_stylesheet_directory() . '/lib/gutenberg/init.php';
}

// Add helper functions.
require_once get_stylesheet_directory() . '/lib/helper-functions.php';

// Add scripts and styles.
require_once get_stylesheet_directory() . '/lib/load-scripts.php';

// Add the blog functions.
require_once get_stylesheet_directory() . '/lib/blog-functions.php';

// Add fonts
function wpb_add_google_fonts() {
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,700;1,600&display=swap', false );
	}
	
	add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );
 


remove_action( 'genesis_footer', 'genesis_do_footer' );
add_action( 'genesis_footer', 'bid_custom_footer' );
function bid_custom_footer() {

	if ( is_front_page() ) {
		echo '<p>Copyright '. do_shortcode( "[footer_copyright]" ) . ' <a href="'.get_bloginfo( 'url' ).'">'.get_bloginfo( 'name' ).'</a> All Rights Reserved <a href="https://thebrandid.com/" target="_blank" class="brandid-logo">Website by The brandiD</a></p>';
	}
	else {
		echo '<p>Copyright '. do_shortcode( "[footer_copyright]" ) . ' <a href="'.get_bloginfo( 'url' ).'">'.get_bloginfo( 'name' ).'</a>  All Rights Reserved</p>';
	}

}
