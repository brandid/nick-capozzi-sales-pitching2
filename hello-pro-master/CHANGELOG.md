# Hello Pro 3 Theme Changelog

## Changelog

### 3.0.6 - (Nov 9, 2020)
* FIXED: Genesis Blocks update! Now the theme uses Genesis Blocks in place of the Atomic Blocks plugin used previously. If you use the One-Click Theme Setup feature, your Demo pages will now be built with Genesis Blocks. We've also migrated our custom Atomic Blocks styles over to the Genesis Blocks plugin.
* FIXED: Forces layout to obey the Customizer setting for "Blog Grid Columns" when the Blog is set to be the Homepage

### 3.0.5 - (Oct 26, 2020)
* FIXED: Allows blog extras to display when the Blog is set to be the Homepage

### 3.0.4 - (May 5, 2020)
* FIXED: Removed repeated footer link CSS styles
* FIXED: Updated a broken link in the One-Click Theme Setup content
* FIXED: Removed unused One-Click Theme Setup blocks on the Blog page (this content doesn't display on Archive pages)

### 3.0.3 - (Feb 5, 2020)
* ADDED: We've added two new optional features to the theme! These new features can be configured using the Customizer:
* - A 'Featured Articles' carousel, which is shown on the Blog archive pages. Showcase your important blog posts in an attractive, responsive carousel slider.
* - A list of your blog Categories, shown on the Blog archive pages. Let users browse further into your blog by displaying a list of categories assigned to your posts.
* FIXED: We've updated the One-Click Theme Setup Demo Homepage and Demo Landing Page to use newer Gutenberg Blocks
* FIXED: We've moved the 'Sticky Header' setting into the 'Hello! Pro 3 Settings' Customizer Panel
* FIXED: Loads of other tweaks to improve the theme!
* See the Hello! Pro 3 theme documentation for more info: https://buildmybrandid.com/docs/hello-pro-3-theme-setup/

### 3.0.2 - (Apr 8, 2019)
* We've added a new "Footer Menu" to the One-Click Demo Install, added page template files, and improved styling for Gutenberg.

### 3.0.1 - (Mar 22, 2019)
Requires Genesis 2.9.0+.
* Package for Initial Release
